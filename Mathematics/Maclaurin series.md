A [[Taylor series]] expansion of a function about zero (when $a = 0$).

![[Taylor series#Formula]]