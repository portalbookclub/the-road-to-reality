The _Taylor series_ of a function is an infinite sum of terms that are expressed in terms of the function's derivatives at a single point. For most common functions, the function and the sum of its _Taylor series_ are equal near this point.

## Formula
$$  
\sum \limits_{n=0}^\infty \frac {{f^{(n)}} (a)}{n!} (x-a)^n
$$

- $a =$	real or complex number
- ${{f^{(n)}} (a)} =$	$n$th derivative of $f$ evaluated at the point $a$
