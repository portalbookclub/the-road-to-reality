The _Estimation lemma_, also known as the ML inequality, gives an upper bound for a contour integral. If $f(z)$ is a complex-valued and continuous function on the contour $\gamma$, and if its absolute value $|f(z)|$ is bounded by a constant $M$ for all $z$ on $\gamma$, then the absolute value of the integral is less than or equal to the length of the contour $M * \gamma$.

## Formula
$$  
 |\oint_{\gamma}f(z)dz| \leq M*l(\gamma)
$$

- $z =$	complex number
- $\gamma =$ contour
-  $M =$ constant
- ${{f(z)}} =$	 complex-valued and continuous function
- $l(\gamma) =$	 real valued arc length of contour $\gamma$
