# Portal Book Club Road to Reality Notes
An [obsidian.md](http://obsidian.md/) vault of notes for **The Road to Reality** by Roger Penrose compiled by Epsilon Group from The Portal Book Club


## Quality of Life
### Plugins
You will want to disable safe mode to activate plugins. Then go to **Settings** `(Ctr+,)` -> **Third-party plugins** and ensure each one is enabled for best experience.

## Command Cheat Sheets
- `git pull` - Pull latest changes into the *branch*
- `git add .` - Stage all file changes
- `git commit -m "description"` - Commit changes with message description
- `git push` - Push changes to remote repository
---

## Index
- [[Chapter 1]]
- [[Chapter 2]]
- [[Chapter 3]]
- [[Chapter 4]]
- [[Chapter 5 Geometry of logarithms, powers and roots]]
- [[Chapter 6 Real-number calculus]]
- [[Chapter 7 Complex-number calculus]]
- [[Chapter 8 Reimann surfaces and complex mappings]]

