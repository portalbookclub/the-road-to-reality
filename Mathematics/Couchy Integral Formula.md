# Formula
$$
\frac{1}{2πi}\oint\frac{f(z)}{z-p}dz=f(p)
$$

# Higher Order Version
$$
\frac{n!}{2πi}\oint\frac{f(z)}{(z-p)^{n+1}}dz=f^{(n)}(p)
$$